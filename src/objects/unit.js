import * as mat4 from 'gl-matrix/src/gl-matrix/mat4';
import {int, mat4Scal, mat4Trans, rand} from '../game/helpers/helpers';
import game from '../game/game';
import Weapon from './weapon';
import Texture from './../game/helpers/texture';
import GameObject, {renderUnit} from './gameobject';
import unitManager from './managers/unitmanager';
import Particle from './../game/helpers/particle';
import levelDraw from './../game/level/levelDraw';
import Vector2D from './../game/helpers/vector';
import AiBot from '../game/ai/aiBot';
import words from './../game/nick/dict';
import Text from '../game/helpers/text';
import FrameBuffer from '../game/helpers/framebuffer';

export default class Unit extends GameObject {
    shot = false;
    alive = true;
    dest = null;
    text = new Text();
    killCount = 0;
    deaths = 0;
    nick = null;
    health = 100;
    beginOfWalk = 0;
    fboText = new FrameBuffer(512, 128);
    fboHealth = new FrameBuffer(64, 50);
    weapon = new Weapon(this, 'fireball');
    collidable = true;
    imageHealth = new Texture('/img/health.png', {wrap: game.gl.CLAMP_TO_EDGE});
    imageBody = new Texture('/img/skins/body.png', {wrap: game.gl.CLAMP_TO_EDGE});
    imageLeg = new Texture('/img/skins/leg.png', {wrap: game.gl.CLAMP_TO_EDGE});
    imageLegBack = new Texture('/img/skins/legback.png', {wrap: game.gl.CLAMP_TO_EDGE});
    textRendered = false;

    getParticle = (type) => unitManager.objects.find((o) => o.type === type && o.died);

    constructor(x, y, speed, angle, size, isBot) {
      super(x, y, speed, angle, size, isBot);
      this.nick = isBot ? words[int(rand(words.length - 1, 0))] : '#rPLAYER';
    }

    respawn = () => {
      this.pos = game.level.getRandomPos();
      this.health = 100;
    };

    calculate = (time) => {
      this.health += 0.05;

      this.health = Math.min(100, this.health);

      if (this.health <= 0) {
        this.deaths++;

        for (let i = 0; i < 10; i++) {
            const norm = new Vector2D(Math.random() * 2 - 1, Math.random() * 2 - 1).mul(Math.random() * 0.05);
          const particle = this.getParticle('gibs') || new Particle('gibs', this.pos);
          particle.type = 'gibs';
          particle.pos.copy(this.pos);
          particle.rnd = Math.random();
          particle.time = Date.now();
          particle.died = false;
          particle.owner = this;
          particle.size.set(1, 1);
          particle.vel = norm.add(norm, this.vel);
          particle.lifetime = 10000;
          particle.omega = (Math.random() * 2 - 1) * 0.03;
          particle.id = (Math.random() * 8) | 0;
        }

        if (this.isBot) {
          this.ai.respawn();
        }

        this.respawn();
      }

        const inRiver = game.level.collideRiver(this.pos) && !game.level.collideBridges(this.pos);

      if (this.isBot) {
          this.speed = inRiver ? game.botSpeed / 1.3 : game.botSpeed;

        this.direction.set(0, 0);
        if (this.key_up) this.direction.add2(0, -this.speed);
        if (this.key_left) this.direction.add2(-this.speed, 0);
        if (this.key_down) this.direction.add2(0, this.speed);
        if (this.key_right) this.direction.add2(this.speed, 0);
      } else {
          this.speed = inRiver ? game.playerSpeed / 1.3 : game.playerSpeed;
      }

      this.angle += this.rotate * (Math.PI / 180);

      const sinA = Math.sin(this.angle);
      const cosA = Math.cos(this.angle);

      this.vel.set(
        (this.direction.x * cosA) + (this.direction.y * sinA),
        (this.direction.y * cosA) - (this.direction.x * sinA)
      ).mul(this.speed);

      if (this.direction.x === 0 && this.direction.y === 0) {
        this.beginOfWalk = 0;
      } else if (this.beginOfWalk === 0) {
        this.beginOfWalk = Date.now();
      }

        if (this.shot) {
            this.weapon.shot();
        }


      if (this.isBot) {
        if (!this.ai) {
          this.ai = new AiBot(this);
        }

        this.ai.update();
      }

      this.pos.add(this.vel);

        game.level.collideUpdate(this, time);

        this.collideObjects();

      return true;
    };

    loaded = () => this.imageBody && this.imageBody.loaded;

    drawHealth = (val) => {
        const sinA = Math.sin(game.camera.angle);
        const cosA = Math.cos(game.camera.angle);

      const pos = this.pos.clone().add2((cosA * 0.1) - sinA, -cosA - (sinA * 0.1));
      this.fboHealth.bind();

      const matPos = mat4.create();
      mat4Trans(matPos, [0, 0, 0]);
      mat4Scal(matPos, [1, 0.5, 1]);

      this.shaderColor.use();
      this.shaderColor.texture(this.shaderColor.tex, this.imageHealth.getId(), 0);
      this.shaderColor.matrix(this.shaderColor.mat_pos, matPos);
      this.shaderColor.vector(this.shaderColor.color, [0, 0, 0, 1]);
      game.gl.drawArrays(game.gl.TRIANGLE_STRIP, 0, 4);


      const matPosBar = mat4.create();
      mat4Trans(matPosBar, [-1 + (val / 100), 0, 0]);
      mat4Scal(matPosBar, [(val / 100), 0.3, 1]);

      this.shaderColor.matrix(this.shaderColor.mat_pos, matPosBar);
      this.shaderColor.vector(this.shaderColor.color, [1 - (val / 100), (val / 100), 0.2, 1]);
      game.gl.drawArrays(game.gl.TRIANGLE_STRIP, 0, 4);

      this.fboHealth.unbind();

      this.render(this.fboHealth, this.shader, null, new Vector2D(2, 1), unitManager.player.angle, pos);
    };

    drawLeg = (val, dx) => {
      const sinA = Math.sin(this.angle);
      const cosA = Math.cos(this.angle);
      const ca = Math.cos(0);
      const sa = Math.sin(0);

      if (val < 0.5) {
        if (val > 0.25) {
          val = 0.5 - val;
        }
        const pos = this.pos.clone().add2(cosA * (dx - val * 2 * sa) - sinA * (-0.3 + val * 2 * ca), -cosA * (-0.3 + val * 2 * ca) - sinA * (dx - val * 2 * sa));

          this.render(this.imageLeg, this.shader, null, new Vector2D(this.size.x, val * 4), this.angle + this.legAngle, pos);
      } else {
        val -= 0.5;
        if (val > 0.25) {
          val = 0.5 - val;
        }
        const pos = this.pos.clone().add2(cosA * (dx + val * 2 * sa) + sinA * (0.2 + val * 2 * ca), cosA * (0.2 + val * 2 * ca) - sinA * (dx + val * 2 * sa));

          this.render(this.imageLegBack, this.shader, null, new Vector2D(this.size.x, val * 4), this.angle + this.legAngle, pos);
      }
    };

    renderNick = () => {
      if (this.nick) {
        if (!this.textRendered) {
          this.fboText.bind();
          this.text.render([-0.1, 0.3], 5, this.nick, 2, {center: true});
          this.fboText.unbind();
          this.textRendered = true;
        }

        const sinA = Math.sin(unitManager.player.angle);
        const cosA = Math.cos(unitManager.player.angle);
        const pos = this.pos.clone().add2((cosA * 0.3) - sinA, -cosA - (sinA * 0.3));
        game.gl.enable(game.gl.BLEND);
        renderUnit(this.fboText.getTexture(), this.shaderText, pos, new Vector2D(5, 3), unitManager.player.angle);
        game.gl.disable(game.gl.BLEND);
      }
    };

    draw = () => {
      game.gl.enable(game.gl.BLEND);

      const pos = this.pos.clone().sub(levelDraw.sun.clone().div(2));
      game.gl.blendFunc(game.gl.DST_COLOR, game.gl.ZERO);
        renderUnit(this.imageBody.getId(), this.shaderShadow, pos, this.size.clone().mul(1.3), this.angle);
      game.gl.blendFunc(game.gl.SRC_ALPHA, game.gl.ONE_MINUS_SRC_ALPHA);

      const period = 200 / this.speed;
      const time = Date.now();
      let step = (time - this.beginOfWalk) / period;

      if (this.beginOfWalk === 0) {
        step = 0;
      }

      let val = step - (step | 0); // from 0 to 1

      // left leg
      this.drawLeg(val, -0.2);
      // right leg
      val += 0.5;
      if (val >= 1) {
        val -= 1;
      }
      this.drawLeg(val, 0.2);

      this.drawHealth(this.health);
      this.render(this.imageBody, this.shader);

      game.gl.disable(game.gl.BLEND);
      this.renderNick();
    };
}

