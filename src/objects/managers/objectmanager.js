export default class ObjectManager {
  globalGuid = 0;
  objects = [];

  init = () => {};

  add = (obj) => {
    this.globalGuid++;
    obj.guid = this.globalGuid;
    this.objects.push(obj);
  };

  remove = (obj) => {
    this.objects = this.objects.filter((o) => o.guid !== obj.guid);
  };

  calculate = () => this.objects.forEach((o) => o.calculate());

  draw = () => {
    this.objects.filter((o) => !!o.renderFirst).forEach((o) => o.draw());
    this.objects.filter((o) => !o.renderFirst).forEach((o) => o.draw());
  }
}
