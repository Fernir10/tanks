import game from '../../game/game';
import Unit from '../unit';
import ObjectManager from './objectmanager';

class UnitManager extends ObjectManager {
  player = null;

  init = () => {
    this.player = new Unit();
    this.player.pos = game.level.getRandomPos();
    this.player.isBot = false;
    this.player.isPlayer = true;
    this.add(this.player);
  };

  loaded = () => !this.objects.some((o) => !o.loaded());
}

const unitManager = new UnitManager();

export default unitManager;
