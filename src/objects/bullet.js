import GameObject from './gameobject';
import game from './../game/game';
import Vector2D from '../game/helpers/vector';
import Texture from './../game/helpers/texture';
import unitManager from './managers/unitmanager';
import Particle from './../game/helpers/particle';

export default class Bullet extends GameObject {
  isBullet = true;
  time = Date.now();
  weapon = null;
    lifetime = 500;

    constructor(pos, angle, weapon) {
        super(pos.x, pos.y, angle, weapon.owner.speed * 5);

    this.weapon = weapon;
    this.dest = this.weapon.owner.dest ? this.weapon.owner.dest.pos : pos.clone().mul(10);
        this.size = new Vector2D();
    this.imageFireball = new Texture('/img/rail.png', {wrap: game.gl.CLAMP_TO_EDGE});
    this.imageFlare = new Texture('/img/fire.png', {wrap: game.gl.CLAMP_TO_EDGE});


    this.vel = (new Vector2D(-Math.sin(angle), -Math.cos(angle))).mul(this.speed);
    unitManager.add(this);
  }

  getParticle = (type) => unitManager.objects.find((o) => o.type === type && o.died);

  putExplode = () => {
    const particle = this.getParticle('explode') || new Particle(this, 'explode');
    particle.type = 'explode';
    particle.size.set(2, 2);
    particle.pos.set(this.pos.x, this.pos.y);
    particle.time = Date.now();
    particle.died = false;
  };

  calculate = () => {
    if (this.died) {
      return;
    }

    if (this.weapon.type === 'fireball' || this.weapon.type === 'flare') {
        this.speed -= this.weapon.owner.speed / 2000;
      this.vel.normalize().mul(this.speed);
      this.pos.add(this.vel);

      const tile = game.level.collideMap(this.pos);

      if (tile) {
        this.vel.sub(tile).normalize().mul(this.speed);
        this.angle = tile.clone().normalize().binormalize().angle();

        this.putExplode();
      }

      unitManager.objects.forEach((unit) => {
        if (unit.collidable && unit !== this && unit !== this.weapon.owner) {
          const norm = this.collide(unit, 0.5);

          if (norm !== null) {
            if (unit !== unitManager.player) {
              unit.health -= 10;

              if (unit.health <= 0) {
                  this.weapon.owner.killCount++;
              }
            }

              this.died = true;
            this.putExplode();
          }
        }
      });


        if (Date.now() > this.time + this.lifetime) {
        this.died = true;
      }
    }
  };

  draw = () => {
    if (this.died) {
      return;
    }

    if (
      !this.imageFireball ||
        !this.imageFireball.ready() ||
        !this.imageFlare ||
        !this.imageFlare.ready()
    ) {
      return;
    }

    game.gl.enable(game.gl.BLEND);
    if (this.weapon.type === 'fireball') {
      this.render(this.imageFireball, this.shaderLight);
    } else if (this.weapon.type === 'flare') {
      game.gl.blendFunc(game.gl.ONE, game.gl.ONE);
      this.render(this.imageFlare, this.shaderLight);
      game.gl.blendFunc(game.gl.SRC_ALPHA, game.gl.ONE_MINUS_SRC_ALPHA);
    }
    game.gl.disable(game.gl.BLEND);
  };
}
