import game from './../game/game';
import Bullet from './bullet';
import unitManager from './managers/unitmanager';
import Vector2D from './../game/helpers/vector';

export default class Weapon {
  nextShoot = 0;
  type = 'fireball';

  attributes = {
    fireball: {
      shotTime: 800,
        lifeTime: 800,
        bulletSpeed: 0.8
    },
    flare: {
      shotTime: 700,
        lifeTime: 700,
        bulletSpeed: 0.7
    }
  };

  constructor(owner, type) {
    this.owner = owner;
    this.type = type;
  }

  shot = () => {
    if (Date.now() > this.nextShoot) {
      const Y = 0.5;
      const angle = this.owner.angle;

      const sinA = Math.sin(angle);
      const cosA = Math.cos(angle);
      const position = this.owner.pos.clone().add2((cosA * 0.01) - (sinA * Y), -(cosA * Y) - (sinA * 0.01));

      // for collision
      const center = position.add(this.owner.pos).mul(0.5);

      if (game.level.getCollide(center) > 128) {
        return;
      }

        const bullet = unitManager.objects.find((o) => o.isBullet && o.died) || new Bullet(position, angle, this);

      bullet.pos.set(position.x, position.y);
      bullet.angle = angle;
      bullet.owner = this.owner;
      bullet.died = false;
        bullet.lifetime = this.attributes[this.type].lifeTime;
      bullet.speed = this.attributes[this.type].bulletSpeed;
      bullet.vel = (new Vector2D(-Math.sin(angle), -Math.cos(angle))).mul(this.speed);
      bullet.collides = 0;
      bullet.time = Date.now();

      bullet.dest = this.owner.dest ? this.owner.dest.pos : this.owner.pos.clone().add2(cosA * 0.2 - sinA * 8, -cosA * 8 - sinA * 0.2);
      bullet.weapon = this;

      this.nextShoot = Date.now() + this.attributes[this.type].shotTime;
      this.owner.lastShootTime = Date.now();
    }
  }
}
