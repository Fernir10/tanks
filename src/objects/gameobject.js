import * as mat4 from 'gl-matrix/src/gl-matrix/mat4';
import {mat4Rotate, mat4Scal, mat4Trans} from './../game/helpers/helpers';
import game from './../game/game';
import Shader, {vertexShader} from './../game/helpers/shader';
import unitManager from './managers/unitmanager';
import levelDraw from './../game/level/levelDraw';
import Vector2D from './../game/helpers/vector';

export const cameraCulling = (pos, size, offsetX = 15, offsetTop = 15, offsetBottom = -9) => {
  const objectRadius = size.length() * 0.5;
  const vec = pos.clone().sub(game.camera.pos).rotate(game.camera.angle);

  return Math.abs(vec.x) - objectRadius > offsetX || (vec.y - objectRadius > offsetTop || vec.y + objectRadius < offsetBottom);
};

export const renderUnit = (image, shader, pos, size, angle, states) => {
  if (cameraCulling(pos, size)) {
    return;
  }

  shader.use();
  shader.texture(shader.tex, image, 0);

  let need_visible = true;
  if (states && states.not_use_visible !== undefined) {
    need_visible = !states.not_use_visible;
  }

  if (need_visible) {
    shader.texture(shader.texVisible, levelDraw.texVisible_id, 1);

    if (game.level.collideRiver(pos) && !game.level.collideBridges(pos)) {
      shader.texture(shader.texVisibleRiver, levelDraw.texRiver_id, 2);
    } else {
      shader.texture(shader.texVisibleRiver, levelDraw.texVisible_id, 2);
    }
    shader.vector(shader.angle, [0, 0, angle, 0]);
  }

  if (states && states.textures !== undefined) {
    states.textures.forEach((tex, index) => {
      shader.texture(tex.location, tex.id, index + 2);
    });
  }

  if (states && states.vectors !== undefined) {
    states.vectors.forEach((vec) => {
      shader.vector(vec.location, vec.vec);
    });
  }

  const aspect = game.canvas.width / game.canvas.height;
  const hRatio = 16 / 9;
  const koef = 1 / 12;

  const matPos = mat4.create();
  mat4Trans(matPos, [0, -game.tankOffset]);

  if (aspect < hRatio) {
    mat4Scal(matPos, [koef / aspect, koef]);
  } else {
    mat4Scal(matPos, [koef / hRatio, koef * (aspect / hRatio)]);
  }

  const mat = mat4.create();
  const vec = pos.clone().sub(game.camera.pos).div(game.zPos);
  mat4Trans(mat, [vec.x, -vec.y]);

  const rotate = mat4.create();
  mat4Rotate(rotate, -game.camera.angle);
  mat4.mul(mat, rotate, mat);
  mat4.mul(matPos, matPos, mat);

  mat4Rotate(matPos, angle);
  mat4Scal(matPos, size.toVec());
  shader.matrix(shader.mat_pos, matPos);

  if (states && states.mat_tex !== undefined) {
    shader.matrix(shader.mat_tex, states.mat_tex);
  }

  if (states && states.mat_pos !== undefined) {
    shader.matrix(shader.mat_pos, states.mat_pos);
  }

  let count = 4;
  if (states && states.vertices_count !== undefined) {
    count = states.vertices_count;
  }

  game.gl.drawArrays(game.gl.TRIANGLE_STRIP, 0, count);
};

export default class GameObject {
  pos = new Vector2D();
    angle = 0.01;
  rotate = 0;
  guid = 0;
  speed = 0;
  shader = null;
  shaderColor = null;
  vert = null;
  size = new Vector2D(1, 1);
  vel = new Vector2D();
  direction = new Vector2D();
  type = null;

  frag = `
    #ifdef GL_ES
    precision highp float;
    #endif

    uniform sampler2D tex;
    uniform vec4 color;
    uniform sampler2D texVisible;
    uniform sampler2D texVisibleRiver;
    varying vec4 texcoord;

    void main()
    {
        vec4 col = texture2D(tex, texcoord.xy);
        vec4 source = vec4(color.rgb, col.r * color.a);
        vec4 visible = texture2D(texVisible, texcoord.zw);
        vec4 visibleRiver = texture2D(texVisibleRiver, texcoord.zw);
        
        float shadow = clamp((1.0 - (visible.g - visibleRiver.b + visibleRiver.g)) * 6.0 - 3.0, 0.7, 1.0);
        
        col.rgb *= ((1.0 - (visible.r + visibleRiver.r)) * shadow);
        
        gl_FragColor = vec4(mix(col.rgb, source.rgb, source.a), max(source.a, col.a));
    }
  `;

  fragColor = `
    #ifdef GL_ES
    precision highp float;
    #endif
    
    uniform sampler2D tex;
    uniform sampler2D texVisible;
    uniform vec4 color;
    varying vec4 texcoord;
    
    void main()
    {
        vec4 col = texture2D(tex, texcoord.xy);
        col *= color;
        gl_FragColor = col;
    }`;

  fragShadow = `
    #ifdef GL_ES
    precision highp float;
    #endif

    varying vec4 texcoord;
    uniform sampler2D tex;
    uniform sampler2D texVisible;

    void main()
    {
        float alpha = texture2D(tex, texcoord.xy).a;
        vec4 visible = texture2D(texVisible, texcoord.zw);
        float shadow = clamp((1.0 - visible.b) * 6.0 - 3.0, 0.2, 1.0);
        shadow = (shadow - 0.5) * 2.0;;
        alpha *= 0.5 * shadow;
        gl_FragColor = vec4(1.0 - alpha);
    }`;

  fragText = `
    #ifdef GL_ES
    precision highp float;
    #endif

    varying vec4 texcoord;
    uniform sampler2D tex;

    void main()
    {
        float alpha = texture2D(tex, texcoord.xy).a;
        gl_FragColor = vec4(alpha);
    }`;

  fragLight = `
    #ifdef GL_ES
    precision highp float;
    #endif

    varying vec4 texcoord;
    uniform sampler2D tex;
    uniform sampler2D texVisible;
    
    float rand(float n){
      return fract(sin(n) * 43758.5453);
    }

    void main()
    {
        vec4 col = texture2D(tex, texcoord.xy);
        vec4 visible = texture2D(texVisible, texcoord.zw);
        
        float shadow = clamp((1.0 - (visible.g)) * 6.0 - 3.0, 0.5, 1.0);
        float alpha = texture2D(tex, texcoord.xy).a;
        
        col.rgb /= shadow;
        gl_FragColor = col;
    }`;


  constructor(x = 0, y = 0, angle = 0, speed = 0.3, size = new Vector2D(1, 1), isBot = false) {
    this.isBot = isBot;
    this.pos.set(x, y);
    this.speed = speed;
    this.angle = angle;
      this.size = size.div(game.zPos + 0.5);

    this.vert = vertexShader(true, false, 'gl_Position');

    this.shader = new Shader(this.vert, this.frag, [
      'mat_pos', 'tex', 'texVisible', 'texVisibleRiver', 'color'
    ]);

    this.shaderColor = new Shader(this.vert, this.fragColor, [
      'mat_pos', 'tex', 'texVisible', 'color'
    ]);

    this.shaderShadow = new Shader(this.vert, this.fragShadow, [
      'mat_pos', 'tex', 'texVisible'
    ]);

    this.shaderText = new Shader(this.vert, this.fragText, [
      'mat_pos', 'tex'
    ]);

    this.shaderLight = new Shader(this.vert, this.fragLight, [
      'mat_pos', 'tex', 'texVisible'
    ]);
  }

  loaded = () => true;

  collide = (unit, size, pos = this.pos) => {
    const minDist = (size || this.size.x);
    const dx = unit.pos.x - pos.x;
    const dy = unit.pos.y - pos.y;
    const len2 = (dx * dx) + (dy * dy);

    return len2 < minDist * minDist ? new Vector2D(dx, dy) : null;
  };

  collideObjects = () => {
    let res = null;

    unitManager.objects.forEach((unit) => {
      if (unit !== this && unit.collidable) {
        const norm = this.collide(unit);

        if (norm !== null) {
          norm.normalize();

          const dot = norm.dot(this.vel);
          if (dot !== 0) {
            const delta = norm.mul(dot);
            dot > 0 ? this.pos.sub(delta) : this.pos.add(delta);
          }
          res = unit;
        }
      }
    });

    return res;
  };

  calculate = () => {};

  draw = () => {};

  render = (image, shader, states, size, angle, pos) =>
    renderUnit(
      image.getId ? image.getId() : image.getTexture(),
      shader,
      pos || this.pos,
      size || this.size,
      angle || this.angle,
      states
    );
}
