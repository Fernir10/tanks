import Vector2D from '../helpers/vector';
import Buffer from '../helpers/buffer';
import game from './../game';

export class WayPoint {
  pos = new Vector2D();

  constructor(pos, dist) {
    this.pos = pos;
    this.dist = dist;
    this.next = [];
    this.deleting_edges = [];
  }

  delNext = (next) => {
    for (let i = 0; i < this.next.length; i++) {
      if (this.next[i] === next) {
        this.next.splice(i, 1);
        break;
      }
    }
  };

  normalize = () => {
    for (let i = 0; i < this.next.length - 1; i++) {
      for (let j = i + 1; j < this.next.length;) {
        if (this.next[i] === this.next[j]) {
          this.next.splice(j, 1);
        } else {
          j++;
        }
      }
    }
  };

  isBridge = () => this.isbridge !== undefined && this.isbridge === true;
}


export default class AI {
  time = Date.now();

  gradientX = null;
  gradientY = null;

  MAX_VALUE = 256;
  POROG = 0.4;
  MAX_DIST = 20;
  waypoints = [];

    OBJECT_VISIBLE_DIST = this.MAX_DIST * 1.4;
  WAYPOINT_VISIBLE_DIST = this.MAX_DIST * 0.5;

  hashMaxDist = null;

  constructor(level) {
    this.obstructionMap = level.level.getObstructionMap();
      this.rawLevel = level.level.getGroundMap();
    this.bridges = level.level.getBridges().bridges;

    this.distanceField = new Buffer(this.obstructionMap.getSize() * 2);
    this.level = new Buffer(this.obstructionMap.getSize() * 2);
    this.level.draw(this.obstructionMap);
    this.distanceField.copy(this.level);

    this.distanceField.normalize(0, this.MAX_VALUE).for_each((val, x, y) => {
      if (x === 0 || x === this.distanceField.getSize() - 1 ||
                y === 0 || y === this.distanceField.getSize() - 1) {
        return this.MAX_VALUE;
      }
      return val;
    });

    this.distanceStepForward();
    this.distanceStepBackward();

    this.gradientX = new Buffer(this.level.getSize());
    this.gradientY = new Buffer(this.level.getSize());

    this.calcGradient();

    this.gradientX.normalize(-1, 1);
    this.gradientY.normalize(-1, 1);

    this.insertBridges();
    this.findWaypoints();
    console.log('Count waypoints = ', this.waypoints.length);
    this.filterNearest();
    console.log('Count waypoints after filter nearest = ', this.waypoints.length);
    this.buildGraph();
    this.filterTrianglePattern();
    this.filterPipirks();
    console.log('Count waypoints after filter pipirks = ', this.waypoints.length);
    this.divCoord();

    this.hashMaxDist = this.calcHash(this.WAYPOINT_VISIBLE_DIST);

    console.log('AI = ', Date.now() - this.time);
  }

  distanceStepForward = () => {
    const size = this.level.getSize();
    const SQRT_2 = Math.sqrt(2);
    for (let j = 1; j < size - 1; j++) {
      for (let i = 1; i < size - 1; i++) {
        if (this.distanceField.getData(i, j) < this.MAX_VALUE - 0.5) {
          const val00 = this.distanceField.getData(i - 1, j - 1) - SQRT_2;
          const val10 = this.distanceField.getData(i, j - 1) - 1;
          const val20 = this.distanceField.getData(i + 1, j - 1) - SQRT_2;
          const val01 = this.distanceField.getData(i - 1, j) - 1;
          const val = Math.max(val00, val10, val20, val01);
          this.distanceField.setData(i + (j * size), val);
        }
      }
    }
  };

  distanceStepBackward = () => {
    const size = this.level.getSize();
    const SQRT_2 = Math.sqrt(2);
    for (let j = size - 1; j > 0; j--) {
      for (let i = size - 1; i > 0; i--) {
        let val = this.distanceField.getData(i, j);
        if (val < this.MAX_VALUE - 0.5) {
          const val00 = this.distanceField.getData(i - 1, j + 1) - SQRT_2;
          const val10 = this.distanceField.getData(i, j + 1) - 1;
          const val20 = this.distanceField.getData(i + 1, j + 1) - SQRT_2;
          const val01 = this.distanceField.getData(i + 1, j) - 1;

          val = Math.max(val00, val10, val20, val01, val);
          this.distanceField.setData(i + (j * size), val);
        }
      }
    }
  };

  calcGradient = () => {
    const size = this.level.getSize();
    for (let j = 1; j < size - 1; j++) {
      for (let i = 1; i < size - 1; i++) {
        const val00 = this.distanceField.getData(i - 1, j - 1);
        const val10 = this.distanceField.getData(i, j - 1);
        const val20 = this.distanceField.getData(i + 1, j - 1);
        const val01 = this.distanceField.getData(i - 1, j);
        const val11 = this.distanceField.getData(i, j);
        const val21 = this.distanceField.getData(i + 1, j);
        const val02 = this.distanceField.getData(i - 1, j + 1);
        const val12 = this.distanceField.getData(i, j + 1);
        const val22 = this.distanceField.getData(i + 1, j + 1);

        const dx = (val00 - val11) + (val01 - val11) + (val02 - val11) +
                    (val11 - val20) + (val11 - val21) + (val11 - val22);

        const dy = (val00 - val11) + (val10 - val11) + (val20 - val11) +
                    (val11 - val02) + (val11 - val12) + (val11 - val22);

        this.gradientX.setData(i + (j * size), dx);
        this.gradientY.setData(i + (j * size), dy);
      }
    }
  };


  insertBridges = () => {
    this.bridges.forEach((bridge) => {
      const center = bridge.pos.clone().mul(2);
      const length = bridge.size.x;
      const dir = new Vector2D(-Math.cos(bridge.angle) * length, Math.sin(bridge.angle) * length);
      const p1 = center.clone().add(dir);
      const p2 = center.clone().sub(dir);
      const way1 = new WayPoint(p1, this.MAX_VALUE);
      const way2 = new WayPoint(p2, this.MAX_VALUE);
      const way3 = new WayPoint(center, this.MAX_VALUE);
      way1.isbridge = true;
      way2.isbridge = true;
      way3.isbridge = true;
      way1.next.push(way3);
      way2.next.push(way3);
      way3.next.push(way1);
      way3.next.push(way2);
      this.waypoints.push(way1);
      this.waypoints.push(way2);
      this.waypoints.push(way3);
    });
  };

  findWaypoints = () => {
    const size = this.level.getSize();
    for (let j = 1; j < size - 1; j++) {
      for (let i = 1; i < size - 1; i++) {
        const val = this.level.getData(i, j);
        if (val > 0.5) {
          continue;
        }

        const valx00 = this.gradientX.getData(i, j);
        const valy00 = this.gradientY.getData(i, j);
        if (valx00 > -this.POROG && valx00 < this.POROG &&
                    valy00 > -this.POROG && valy00 < this.POROG) {
          this.waypoints.push(new WayPoint(new Vector2D(i, j), this.MAX_VALUE - this.distanceField.getData(i, j)));
        }

        const valx10 = this.gradientX.getData(i + 1, j);
        const valx01 = this.gradientX.getData(i, j + 1);
        const valx11 = this.gradientX.getData(i + 1, j + 1);

        const valy10 = this.gradientY.getData(i + 1, j);
        const valy01 = this.gradientY.getData(i, j + 1);
        const valy11 = this.gradientY.getData(i + 1, j + 1);

        const valx = valx00 + valx10 + valx01 + valx11;
        const valy = valy00 + valy10 + valy01 + valy11;
        if (valx > -this.POROG && valx < this.POROG &&
                    valy > -this.POROG && valy < this.POROG) {
          this.waypoints.push(new WayPoint(new Vector2D(i + 0.5, j + 0.5), this.MAX_VALUE - this.distanceField.getData(i, j)));
        }
      }
    }
    this.waypoints.sort((a, b) => b.dist - a.dist);
  };

  calcHash = (sizeCeil) => {
    const hash = [];
    for (let i = 0; i < this.waypoints.length; i++) {
      const x = (this.waypoints[i].pos.x / sizeCeil) | 0;
      const y = (this.waypoints[i].pos.y / sizeCeil) | 0;
      const ind = x + (this.level.getSize() * y);
      if (hash[ind] === undefined) hash[ind] = [];
      hash[ind].push(this.waypoints[i]);
    }
    return hash;
  };

  hashForEach = (hash, waypoint, sizeCeil, callback) => {
    const x = (waypoint.pos.x / sizeCeil) | 0;
    const y = (waypoint.pos.y / sizeCeil) | 0;

    for (let xx = x - 1; xx <= x + 1; xx++) {
      for (let yy = y - 1; yy <= y + 1; yy++) {
        const ind = xx + (yy * this.level.getSize());
        if (hash[ind]) {
          hash[ind].forEach((next) => {
            if (next === waypoint) { return; }

            callback(next);
          });
        }
      }
    }
  };

  deleteWaypoints = () => {
    for (let i = 0; i < this.waypoints.length;) {
      if (this.waypoints[i].del && this.waypoints[i].del === true) {
        this.waypoints[i].next.forEach((next) => {
          next.delNext(this.waypoints[i]);
        });
        this.waypoints.splice(i, 1);
      } else {
        i++;
      }
    }
  };

  filterNearest = () => {
    const MIN_DIST = 4;
    const hash = this.calcHash(MIN_DIST);

    for (let i = 0; i < this.waypoints.length; i++) {
      const cur = this.waypoints[i];

      if (cur.del && cur.del === true) {
        continue;
      }
      if (cur.isBridge()) {
        continue;
      }

      this.hashForEach(hash, cur, MIN_DIST, (next) => {
        if (next.isBridge()) {
          return;
        }

        const dir = cur.pos.clone().sub(next.pos);
        if (dir.clone().dot(dir) < MIN_DIST * MIN_DIST) {
          next.del = true;
        }
      });
    }
    this.deleteWaypoints();
  };

  visible = (a, b, buffer, maxVal, maxDist) => {
    const norm = b.clone().sub(a);
    const len = norm.length();
    if (len > maxDist) {
      return false;
    }

    // tracing there
    norm.normalize();
    for (let step = 1; step < len; step++) {
      const vec = norm.clone().mul(step);
      const pos = a.clone().add(vec);
      const x = (pos.x + 0.5) | 0;
      const y = (pos.y + 0.5) | 0;
      if (buffer.getData(x, y) > maxVal) {
        return false;
      }
    }
    return true;
  };

  buildGraph = () => {
    const hash = this.calcHash(this.MAX_DIST);
    for (let i = 0; i < this.waypoints.length; i++) {
      const cur = this.waypoints[i];

      this.hashForEach(hash, cur, this.MAX_DIST, (next) => {
        if (this.visible(cur.pos, next.pos, this.level, 80, this.MAX_DIST)) {
          cur.next.push(next);
          next.next.push(cur);
        }
      });
    }

    this.waypoints.forEach((w) => w.normalize());
  };

  filterTrianglePattern = () => {
    const equals = (next1, next2) => {
      const ret = [];
      for (let i = 0; i < next1.length; i++) {
        const n1 = next1[i];
        for (let j = 0; j < next2.length; j++) {
          const n2 = next2[j];
          if (n1 === n2) { ret.push(n1); }
        }
      }
      return ret;
    };

    for (let i = 0; i < this.waypoints.length; i++) {
      const cur = this.waypoints[i];

      for (let ii = 0; ii < cur.next.length; ii++) {
        const next = cur.next[ii];
        const commons = equals(cur.next, next.next);
        for (let jj = 0; jj < commons.length; jj++) {
          const common = commons[jj];
          // triangle A - cur; B - next; C - common
          const ABx = next.pos.x - cur.pos.x;
          const ABy = next.pos.y - cur.pos.y;
          const ACx = common.pos.x - cur.pos.x;
          const ACy = common.pos.y - cur.pos.y;
          const BCx = common.pos.x - next.pos.x;
          const BCy = common.pos.y - next.pos.y;
          const ab = ABx * ABx + ABy * ABy;
          const ac = ACx * ACx + ACy * ACy;
          const bc = BCx * BCx + BCy * BCy;

          if (ab > ac && ab > bc) {
            cur.deleting_edges.push(next);
          } else if (ac > ab && ac > bc) {
            cur.deleting_edges.push(common);
          }
        }
      }
    }
    for (let i = 0; i < this.waypoints.length; i++) {
      const cur = this.waypoints[i];
      cur.deleting_edges.forEach((del) => {
        cur.delNext(del);
        del.delNext(cur);
      });
    }
  };


  filterPipirks = () => {
    for (let i = 0; i < this.waypoints.length; i++) {
      const cur = this.waypoints[i];

      if (cur.isBridge()) {
        continue;
      }

      if (cur.next.length <= 1) {
        cur.del = true;
      }
    }
    this.deleteWaypoints();
  };

  divCoord = () => {
    for (let i = 0; i < this.waypoints.length; i++) {
      this.waypoints[i].pos.mul(0.5);
      this.waypoints[i].dist *= 0.5;
    }
  };

  isVisible = (myPos, pos, val = 2.5, maxDist = this.WAYPOINT_VISIBLE_DIST) => {
    const a = myPos.clone().mul(2);
    const b = pos.clone().mul(2);
    return this.visible(a, b, this.distanceField, this.MAX_VALUE - val, maxDist * 2);
  };

  botVisible = (myPos, botPos) => this.visible(myPos, botPos, this.rawLevel, 80, this.OBJECT_VISIBLE_DIST);

  getVisibleWaypoint = (unit) => {
    const ret = [];
    this.hashForEach(this.hashMaxDist, unit, this.WAYPOINT_VISIBLE_DIST, (next) => {
      if (this.isVisible(unit.pos, next.pos, 0.5)) {
        ret.push(next);
      }
    });
    return ret;
  };

  getGradient = (myPos) => {
    const x = myPos.x * 2 | 0;
    const y = myPos.y * 2 | 0;

    if (this.level.getData(x, y) > 0.5) { // lava
      const bridge = game.level.collideBridges(myPos);

      if (bridge) {
        return new Vector2D(-Math.cos(bridge.bridge.angle), Math.sin(bridge.bridge.angle));
      }

      // console.log('Could not find bridge');
      return new Vector2D(1, 0);
    }

    const gradX = this.gradientX.getData(x, y);
    const gradY = this.gradientY.getData(x, y);
    const vec = new Vector2D(gradX, gradY);
    return vec.normalize();
  };
}
