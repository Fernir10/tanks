import Vector2D from '../helpers/vector';
import game from './../game';
import {cameraCulling} from './../../objects/gameobject';
import {normalizeAngle} from './../../game/helpers/helpers';

export class Forbidden {
  constructor(maxCount) {
    this.max_count = maxCount;
    this.waypoints = [];
  }

    push = (way) => {
      this.waypoints.push(way);
      if (this.waypoints.length > this.max_count) {
        this.waypoints.splice(0, 1);
      }
    };

    clear = () => {
      this.waypoints.splice(0, this.waypoints.length);
    };

    check = (way) => {
      for (let i = 0; i < this.waypoints.length; i++) {
        if (this.waypoints[i] === way) {
          return true;
        }
      }
      return false;
    };
}


export default class AiBot {
    OBJECT_VISIBLE_OFFSET_X = 15;
    OBJECT_VISIBLE_OFFSET_TOP = 11;
    OBJECT_VISIBLE_OFFSET_BOTTOM = -9;

    // think
    STATE_AFTER_RESPAWN = 'STATE_AFTER_RESPAWN';
    STATE_FIND_MASTER = 'STATE_FIND_MASTER';
    STATE_CHECK_NEXT = 'STATE_CHECK_NEXT';
    STATE_CHECK_OBJECT = 'STATE_CHECK_OBJECT';

    // movement
    STATE_MOVE_STAY = 0;
    STATE_MOVE_TO_MASTER = 1;
    STATE_MOVE_TO_POINT = 2;
    STATE_MOVE_TO_POINT_SAFE = 3;
    STATE_MOVE_GRADIENT = 4;
    STATE_ATTACK = 5;

    // head
    STATE_HEAD_STAY = 0;
    STATE_HEAD_FRONT = 1;
    STATE_HEAD_WAYPOINT = 2;
    STATE_HEAD_SMOOTH_WAYPOINT = 3;
    STATE_HEAD_POINT = 4;
    STATE_HEAD_SHOOTED = 5;
    STATE_HEAD_BOT = 6;

    AI = null;

    constructor(owner) {
        this.AI = game.level.getAI();
      this.owner = owner;
      this.reaction_time = 200 + (Math.random() * 300);
      this.angle_speed = 1 + Math.random();
        this.max_angle_speed = 2 + Math.random();
      this.respawn();
    }

    respawn = () => {
      this.state = this.STATE_AFTER_RESPAWN;
      this.state_move = this.STATE_MOVE_STAY;
      this.state_head = this.STATE_HEAD_STAY;
      this.reaction = Date.now() + this.reaction_time;
      this.item = null;
      this.bot = null;
      this.bot_last_visible_time = 0;
      this.shooted_bot = null;
      this.danger_pos = null;
      this.point = null;
      this.bot_point = null;
      this.waypoint_master = null;
      this.waypoint_next = null;
      this.diff = null;
      this.forbidden = null;
      this.attack_point = null;
    };

    stay = () => {
        this.owner.key_up = false;
        this.owner.key_left = false;
        this.owner.key_down = false;
        this.owner.key_right = false;
    };

    moveTo = (pos) => {
        const dir = pos.clone().sub(this.owner.pos);

        if (dir.dot(dir) < 0.25 * 0.25) {
            return dir;
        }

        this.stay();

        dir.normalize().rotate(this.owner.angle);

        if (dir.x > Math.cos((Math.PI / 4) + (Math.PI / 8))) {
            this.owner.key_right = true;
        }

        if (dir.x < -Math.cos((Math.PI / 4) + (Math.PI / 8))) {
            this.owner.key_left = true;
        }

        if (dir.y < -Math.sin((Math.PI / 4) - (Math.PI / 8))) {
            this.owner.key_down = true;
        }

        if (dir.y > Math.sin((Math.PI / 4) - (Math.PI / 8))) {
            this.owner.key_up = true;
        }

        return null;
    };

    angleTo = (pos) => {
        const dirToPos = pos.clone().sub(this.owner.pos);
        const angle = normalizeAngle(dirToPos.angle() - (Math.PI / 2));
        let delta = normalizeAngle(angle - this.owner.angle);
        if (delta > Math.PI) delta -= 2 * Math.PI;
        let updateAngle = delta * (this.angle_speed);
        if (updateAngle > this.max_angle_speed / 20) updateAngle = this.max_angle_speed / 20;
        if (updateAngle < -this.max_angle_speed / 20) updateAngle = -this.max_angle_speed / 20;
        this.owner.angle = normalizeAngle(this.owner.angle + updateAngle);
        return delta;
    };

    botVisible = (unit) => {
        if (this.owner.isPlayer) {
            if (cameraCulling(unit.pos, unit.size,
                this.OBJECT_VISIBLE_OFFSET_X, this.OBJECT_VISIBLE_OFFSET_TOP, this.OBJECT_VISIBLE_OFFSET_BOTTOM)) {
                return false;
            }
        }

        return this.AI.botVisible(this.owner.pos, unit.pos);
    };

    findShootedBot = () => {
        if (this.bot) {
            return false;
        }

        for (let i = 0; i < game.bots().length; i++) {
            const bot = game.bots()[i];
            if (bot === this.owner || !bot.alive) {
                continue;
            }

            if (Date.now() > bot.last_shoot_time + 500) {
                continue;
            }

            if (this.AI.botVisible(this.owner.pos, bot.pos)) {
                this.shooted_bot = bot;
                return true;
            }
        }

        return false;
    };

    findBullet = () => {
        const a = this.owner.pos;
        const v = this.owner.vel;
        let dangerPos = null;
        let dangerTime = 5000;
        for (let i = 0; i < game.bullets().length; i++) {
            const bullet = game.bullets()[i];
            if (bullet.owner !== this.owner) {
                if (!this.botVisible(bullet)) {
                    continue;
                }

                const b = bullet.pos;
                const w = bullet.vel;
                const t = (a.dot(w) + (b.dot(v) - a.dot(v) - b.dot(w))) / v.clone().sub(w).dot(v.clone().sub(w));
                const A = a.clone().add(v.clone().mul(t));
                const B = b.clone().add(w.clone().mul(t));
                const distance = A.clone().sub(B).dot(A.clone().sub(B));

                if (distance < 0 && t < dangerTime) {
                    dangerTime = t;
                    dangerPos = B;
                }
            }
        }

        if (dangerPos) {
            this.danger_pos = dangerPos;
            return true;
        }

        return false;
    };

    findAttackingMeBot = () => {
        const bots = game.bots();
        const foundBot = bots.find((bot) => bot.owner !== this.owner && bot.ai.bot === this.owner && this.botVisible(bot));

        if (foundBot) {
            this.bot = foundBot;
            this.danger_pos = foundBot.pos;
            return true;
        }

        return false;
    };

    findBot = () => {
        if (this.bot) {
            if (this.botVisible(this.bot)) {
                this.bot_last_visible_time = Date.now();
                return true;
            }

            if (Date.now() < this.bot_last_visible_time + 500) {
                return true;
            }
        }

        for (let i = 0; i < game.bots().length; i++) {
            const bot = game.bots()[i];
            if (bot === this.owner || !bot.alive) {
                continue;
            }

            if (this.botVisible(bot)) {
                this.bot_last_visible_time = Date.now();
                this.bot = bot;
                return true;
            }
        }
        return false;
    };

    findObject = () => {
        const botFinded = this.findBot();
        const bulFinded = this.findBullet();
        const shbFinded = this.findShootedBot();
        const botAttackingMe = this.findAttackingMeBot();

        if (botFinded || bulFinded) {
            this.state = this.STATE_CHECK_OBJECT;
        }

        const myPos = this.owner.pos;

        if (bulFinded) {
            this.point = myPos.clone().add(myPos.clone().sub(this.danger_pos).normalize());
            this.state_move = this.STATE_MOVE_TO_POINT_SAFE;
            // this.state_head = this.STATE_HEAD_SMOOTH_WAYPOINT;
            return;
        }

        if (botAttackingMe && this.owner.health < this.bot.health) {
            this.point = myPos.clone().add(myPos.clone().sub(botFinded.pos).normalize());
            this.state_move = this.STATE_MOVE_TO_POINT_SAFE;
            // this.state_head = this.STATE_HEAD_SMOOTH_WAYPOINT;
        } else if (botFinded) {
            this.state_move = this.STATE_ATTACK;
        }

        if (botFinded) {
            this.bot_point = {
                pos: this.bot.pos.clone(),
                vel: this.bot.vel.clone(),
                time: Date.now()
            };

            this.owner.dest = this.bot_point;

            this.state_head = this.STATE_HEAD_BOT;
        } else if (shbFinded) {
            this.state_head = this.STATE_HEAD_SHOOTED;
            this.point_head = this.shooted_bot.pos.clone();
        }

        return botFinded || bulFinded;
    };

    getMostFronted = (ways) => {
        const pos = this.owner.pos;
        const dir = new Vector2D(-Math.sin(this.owner.angle), -Math.cos(this.owner.angle));
        let maxDot = -2;
        let wayWithMaxDot = null;
        ways.forEach((way) => {
            const to = way.pos.clone().sub(pos).normalize();
            const dot = to.dot(dir);
            if (dot > maxDot) {
                maxDot = dot;
                wayWithMaxDot = way;
            }
        });
        return wayWithMaxDot;
    };

    chooseNext = (frontedNext, protect) => {
        const next = [];

        this.waypoint_master.next.forEach((n) => {
            if (!this.forbidden.check(n)) {
                next.push(n);
            }
        });

        if (next.length === 0) {
            console.assert(protect === undefined);
            this.forbidden.clear();
            // this.chooseNext(frontedNext, 'protect');
        } else if (frontedNext) {
            this.waypoint_next = this.getMostFronted(next);
        } else {
            this.waypoint_next = next[(Math.random() * next.length) >> 0];
        }
    };

    checkNext = () => {
        const myPos = this.owner.pos;
        const dirToMaster = myPos.clone().sub(this.waypoint_master.pos);
        const len = dirToMaster.dot(dirToMaster);

        if (this.waypoint_next) {
            if (len < 1 || this.AI.isVisible(myPos, this.waypoint_next.pos)) {
                this.forbidden.push(this.waypoint_master);
                this.waypoint_master = this.waypoint_next;

                const radius = this.waypoint_master.isBridge() ? 1 : this.waypoint_master.dist;
                this.diff = (new Vector2D(radius, 0)).rotate(Math.PI * 2);
                this.chooseNext(false);
                return true;
            }
        } else {
            this.chooseNext(true, 'protect');
        }

        return false;
    };

    resetMaster = (way) => {
        this.waypoint_master = way;
        this.forbidden = new Forbidden(5);
        this.diff = null;
        this.chooseNext(true);
        this.state_move = this.STATE_MOVE_TO_MASTER;
        this.state_head = this.STATE_HEAD_SMOOTH_WAYPOINT;
        this.state = this.STATE_CHECK_NEXT;
    };

    resetState = () => {
        const ways = this.AI.getVisibleWaypoint(this.owner);
        if (ways.length > 0) {
            const way = this.getMostFronted(ways);
            // console.assert(way);
            this.resetMaster(way);
        } else {
            this.state = this.STATE_FIND_MASTER;
        }
    };

    safeMove = () => {
        const pos = this.owner.pos.clone();
        const safe = game.level.getSafetyDir(pos);
        if (safe) {
            const dir = this.owner.vel.clone().normalize();

            if (dir.dot(safe) > 0) {
                const randomVector = (new Vector2D((2 * Math.random()) - 1, (2 * Math.random()) - 1));
                pos.add(safe.mul(-1).normalize().add(randomVector));
                return pos;
            }
        }

        return null;
    };

    moveToPoint = (point) => {
        const dir = this.moveTo(point);

        if (dir) {
            point.add(dir.normalize());
        }
    };

    update = () => {
        let aiUpdate = false;

        if (Date.now() > this.reaction) {
            aiUpdate = true;
            this.reaction = Date.now() + this.reaction_time;
        }


        switch (this.state) {
            case this.STATE_AFTER_RESPAWN: {
                this.state_move = this.STATE_MOVE_STAY;
                this.state_head = this.STATE_HEAD_STAY;

                if (aiUpdate) {
                    this.state = this.STATE_FIND_MASTER;
                }
                break;
            }
            case this.STATE_FIND_MASTER: {
                this.state_move = this.STATE_MOVE_GRADIENT;
                this.state_head = this.STATE_HEAD_FRONT;
                const ways = this.AI.getVisibleWaypoint(this.owner);
                if (ways.length > 0) {
                    this.resetMaster(ways[(Math.random() * ways.length) >> 0]);
                } else if (aiUpdate) {
                    this.findObject();
                }
                break;
            }
            case this.STATE_CHECK_NEXT: {
                this.checkNext();

                if (aiUpdate) {
                    this.findObject();
                }
                break;
            }
            case this.STATE_CHECK_OBJECT: {
                if (aiUpdate) {
                    if (this.bot && !this.bot.alive) {
                        this.bot = null;
                    }
                    if (this.item && !this.item.alive) {
                        this.item = null;
                    }
                    if (!this.findObject()) {
                        this.resetState();
                    }
                }
                break;
            }

            default:
        }

        switch (this.state_move) {
            case this.STATE_MOVE_STAY:
                this.stay();
                break;
            case this.STATE_MOVE_TO_MASTER: {
                const vec = this.waypoint_master.pos.clone();

                if (this.diff) {
                    const dir = vec.clone().sub(this.owner.pos);
                    const len = dir.length() - 1;
                    const radius = this.diff.length();

                    if (len < radius) {
                        vec.add(this.diff.clone().mul(len / radius));
                    }
                }
                this.moveTo(vec);

                break;
            }
            case this.STATE_MOVE_TO_POINT: {
                this.moveToPoint(this.point);
                break;
            }
            case this.STATE_MOVE_TO_POINT_SAFE: {
                const pos = this.safeMove();

                if (pos) {
                    this.point = pos;
                }

                this.moveToPoint(this.point);
                break;
            }
            case this.STATE_MOVE_GRADIENT: {
                const myPos = this.owner.pos.clone();
                const grad = this.AI.getGradient(myPos);
                myPos.add(grad);
                this.moveTo(myPos);
                break;
            }
            case this.STATE_ATTACK: {
                this.attack_point = this.bot_point.pos.clone();

                const pos = this.safeMove();

                if (pos) {
                    this.attack_point = pos;
                }

                if (this.attack_point && this.attack_point.dist(this.owner.pos) < 6) {
                    this.state_move = this.STATE_HEAD_STAY;
                    return;
                } else if (this.attack_point && this.attack_point.dist(this.owner.pos) > 6) {
                    this.moveToPoint(this.attack_point);
                }

                break;
            }
            default:
        }

        this.owner.shot = false;
        switch (this.state_head) {
            case this.STATE_HEAD_STAY:
                break;
            case this.STATE_HEAD_FRONT: {
                const pos = this.owner.pos.clone().add(this.owner.direction);
                this.angleTo(pos);
                break;
            }
            case this.STATE_HEAD_WAYPOINT: {
                const pos = this.waypoint_next ? this.waypoint_next.pos : this.waypoint_master.pos;
                this.angleTo(pos);
                break;
            }
            case this.STATE_HEAD_SMOOTH_WAYPOINT: {
                const pos = this.waypoint_next ? this.waypoint_next.pos : this.waypoint_master.pos;
                const delta = this.angleTo(pos);
                if (Math.abs(delta) < Math.PI / 6) {
                    this.state_head = this.STATE_HEAD_WAYPOINT;
                }
                break;
            }
            case this.STATE_HEAD_POINT: {
                this.angleTo(this.point_head);
                break;
            }
            case this.STATE_HEAD_SHOOTED: {
                const delta = this.angleTo(this.point_head);
                if (Math.abs(delta) < Math.PI / 12) {
                    if (this.waypoint_next || this.waypoint_master) {
                        this.state_head = this.STATE_HEAD_SMOOTH_WAYPOINT;
                    } else {
                        // console.log('Wyapoint == null');
                        this.state_head = this.STATE_HEAD_FRONT;
                    }
                }
                break;
            }
            case this.STATE_HEAD_BOT: {
                const myPos = this.owner.pos;
                const delta = Date.now() - this.bot_point.time;
                const botSpeed = this.bot_point.vel.clone().mul(delta);
                const botPos = this.bot_point.pos.clone().add(botSpeed);

                this.angleTo(botPos, this.angle_speed * 3);
                const myDir = new Vector2D(Math.sin(this.owner.angle), Math.cos(this.owner.angle));
                const dirToBot = botPos.clone().sub(myPos);
                const binorm = dirToBot.clone().normalize(dirToBot).binormalize();
                const rast = binorm.dot(myDir) * dirToBot.length();

                const needShoot = rast > -0.4 && rast < 0.4 && (this.attack_point && this.attack_point.dist(this.owner.pos) < 6);

                if (needShoot) {
                    this.owner.shot = this.AI.botVisible(this.owner.pos, botPos);
                }
                break;
            }
            default:
        }
    }
}
