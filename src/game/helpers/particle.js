import game from './../../game/game';
import GameObject from './../../objects/gameobject';
import Texture from './../../game/helpers/texture';
import Shader from './../../game/helpers/shader';
import unitManager from './../../objects/managers/unitmanager';
import Vector2D from './vector';
import Buffer, {createTexture} from './buffer';
import {renderDecal} from './decal';

export const createSplash = () => {
  const COUNT_PART = 256;
  const SIZE = 70;
  const LENGTH = 16;

  const ret = [];
  const pos = new Array(COUNT_PART);
  const vel = new Array(COUNT_PART);
  const len = new Array(COUNT_PART);

  for (let i = 0; i < COUNT_PART; i++) {
    const sx = (Math.random() * 2 - 1);
    const sy = (Math.random() * 2 - 1);
    const px = SIZE * 0.5 + Math.random() * sx * 16 + sx;
    const py = SIZE * 0.5 + Math.random() * sy * 16 + sy;
    pos[i] = new Vector2D(px, py);
    vel[i] = new Vector2D(sx, sy).normalize().mul(0.6);
    len[i] = LENGTH;
  }

  for (let i = 0; i < 32; i++) {
    const buf = new Buffer(SIZE);
    for (let j = 0; j < COUNT_PART; j++) {
        const x = pos[j].x >> 0;
        const y = pos[j].y >> 0;
      const koef = (32 - i) / 32 * 2;
        buf.bresenham(x, y, (x - vel[j].x * len[j]) >> 0, (y - vel[j].y * len[j]) >> 0, koef);
      pos[j].add(vel[j]);
    }
    const bluredBuffer = buf.getGaussian(6).clamp(0, 1);
    const clampedBuffer = new Buffer(SIZE);

    clampedBuffer.copy(bluredBuffer);
    clampedBuffer.clamp(0, 0.2).normalize(0, 1);
    ret.push(createTexture(clampedBuffer, bluredBuffer, bluredBuffer, bluredBuffer, {wrap: game.gl.CLAMP_TO_EDGE}));
  }

  return ret;
};

export default class Particle extends GameObject {
  type = null;
  time = 0;
  renderFirst = true;
  lifetime = 0;
  image = new Texture('/img/explode.png');
  shaderExplode = null;
  particles = [];
  texGibs = null;
  shaderGibs = null;

  vertExplode = `
    attribute vec4 position;
    uniform mat4 mat_pos;
    uniform vec4 dtc;
    varying vec4 texcoord;
    varying vec4 koef;
    
    void main()
    {
        gl_Position = mat_pos * position;
        texcoord.xy = position.xy * 0.5 + 0.5;
        texcoord.xy = texcoord.xy * 0.25 + dtc.xy;
        texcoord.zw = gl_Position.xy * 0.5 + 0.5;
        koef = dtc.zzzz;
    }`;

  fragExplode = `
    #ifdef GL_ES
    precision highp float;
    #endif
    
    uniform sampler2D tex;
    uniform sampler2D tex_visible;
    varying vec4 texcoord;
    varying vec4 koef;
    
    void main()
    {
        vec4 col = texture2D(tex, texcoord.xy);
        vec4 visible = texture2D(tex_visible, texcoord.zw);
        col *= 1.0 - visible.r;
        col.a = (col.r + col.g + col.b);
        gl_FragColor = col;
    }`;

  fragColor = `
    #ifdef GL_ES
    precision highp float;
    #endif
    
    uniform sampler2D tex;
    uniform sampler2D texVisible;
    uniform vec4 color;
    varying vec4 texcoord;
    
    void main()
    {
        vec4 col = texture2D(tex, texcoord.xy);
        vec4 visible = texture2D(texVisible, texcoord.zw);
        float shadow = clamp((1.0 - visible.g) * 6.0 - 3.0, 0.5, 1.0);
        col *= (1.0 - visible.r) * color;
        gl_FragColor = col;
    }`;

  constructor(owner, type, pos = new Vector2D(), dir = new Vector2D(), count = 0) {
    super(pos.x, pos.y, 0, Math.random() * 10);

    this.owner = owner;
    this.count = count;
    this.type = type;
    this.time = Date.now();
    this.lifetime = 400;

    this.texGibs = new Texture('/img/gibs.png');

    this.shaderExplode = new Shader(this.vertExplode, this.fragExplode, [
      'mat_pos', 'dtc', 'tex', 'tex_visible'
    ]);

    this.shaderGibs = new Shader(this.vertExplode, this.fragColor, [
      'mat_pos', 'dtc', 'tex', 'texVisible', 'color'
    ]);

    for (let i = 0; i < this.count; i++) {
      this.particles.push(new Particle(type, pos, dir));
    }

    unitManager.add(this);
  }

  calculate = () => {
    if (this.died) {
      return;
    }

    if (Date.now() - this.time > this.lifetime) {
      this.died = true;
      this.particles.forEach((p) => { p.died = true; });
    }

    if (this.type === 'gibs') {
      this.angle += this.omega;

        this.vel.mul(0.95);
        this.omega *= 0.95;

      // collide map
      const norm = game.level.collideMap(this.pos);

      if (norm) {
        const dot = norm.normalize().dot(this.vel);

        if (dot > 0) {
          const reflect = norm.mul(2 * dot);
          this.vel.sub(reflect);
          this.omega *= -1;
        }
      }

      this.pos.add(this.vel);
    }
  };

    calcCadr = (count) => Math.min(count, ((Date.now() - this.time) * (count / this.lifetime)) >> 0);

  draw = () => {
    if (this.died) {
      return;
    }

    if (!this.image || !this.image.ready()) {
      return;
    }

    game.gl.enable(game.gl.BLEND);

    if (this.type === 'explode') {
        const kadr = this.calcCadr(7);
        const kadr2 = this.calcCadr(31);
      const sx = kadr % 2 ? 0.25 : 0;
        const sy = 1 - (((kadr * 0.5) >> 0) * 0.25);

      this.render(this.image, this.shaderExplode, {
          vectors: [{location: this.shaderExplode.dtc, vec: [sx + 1, sy - 0.25, 1, 0]}]
      });
      renderDecal(this, game.splashTextures[kadr2], [0, 0, 0, 0.8]);
    } else if (this.type === 'gibs') {
        const sx = ((this.id % 4) >> 0) * 0.25;
        const sy = (((this.id / 4) >> 0) * 0.25) + 0.5;
        const alpha = 1 - (this.calcCadr(100) / 100);
        const rnd = this.rnd * 0.5;
        const kadr = this.calcCadr(31);

      this.render(this.texGibs, this.shaderGibs, {vectors: [
        {location: this.shaderGibs.dtc, vec: [sx, sy, 0, 0]},
              {location: this.shaderGibs.color, vec: [1, 0, 0, alpha]}
      ]});

        renderDecal(this, game.splashTextures[kadr], [0.5 + rnd, 0, 0, alpha]);
    }

    game.gl.disable(game.gl.BLEND);
  };
}
