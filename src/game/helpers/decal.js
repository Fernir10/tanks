import * as mat4 from 'gl-matrix/src/gl-matrix/mat4';
import Shader, {vertexShader} from './shader';
import FrameBuffer from './framebuffer';
import Vector2D from './vector';
import game from '../game';
import {mat4Rotate, mat4Scal, mat4Trans} from './helpers';
import levelDraw from '../level/levelDraw';
import {renderUnit} from '../../objects/gameobject';

export default class Decal {
    fragBlit = `
    #ifdef GL_ES
    precision highp float;
    #endif
    
    uniform sampler2D tex;
    varying vec4 texcoord;
    
    void main() { gl_FragColor = texture2D(tex, texcoord.zw); }
   `;
    fragLerp = `
    #ifdef GL_ES
    precision highp float;
    #endif
    
    uniform sampler2D tex;
    uniform sampler2D tex_dest;
    uniform vec4 color;
    varying vec4 texcoord;
    
    void main()
    {
        vec4 dest = texture2D(tex_dest, texcoord.zw);
        vec4 col = texture2D(tex, texcoord.xy);
        vec4 source = vec4(color.rgb, col.r * color.a);
        gl_FragColor = vec4(mix(dest.rgb, source.rgb, source.a), max(source.a, dest.a));
    }
   `;

    fragAdd = `
    #ifdef GL_ES
    precision highp float;
    #endif
    
    uniform sampler2D tex;
    uniform sampler2D tex_dest;
    uniform vec4 color;
    varying vec4 texcoord;
    
    void main()
    {
        vec4 dest = texture2D(tex_dest, texcoord.zw);
        vec4 col = texture2D(tex, texcoord.xy);
        vec4 source = color * col.rrrr;
        gl_FragColor = dest + source;
    }
   `;

    fragDrying = `
    #ifdef GL_ES
    precision highp float;
    #endif
    
    uniform sampler2D tex;
    varying vec4 texcoord;
    
    void main()
    {
        vec4 col = texture2D(tex, texcoord.xy);
        gl_FragColor = col * 0.99;
    }
   `;

  vert = vertexShader(true, false, 'gl_Position');
  fbo_decals = null;
  fbo_decal = null;
  fbo_dry = null;
  current_dry_fbo = null;
  size_quad = 32;

  shader_blit = null;
  shader_lerp = null;
  shader_add = null;
  shader_drying = null;

  constructor(size) {
      const arrSize = size / this.size_quad;
    this.fbo_decals = new Array(arrSize * arrSize);
    this.fbo_decal = new FrameBuffer(512, 512);
    this.fbo_dry = new FrameBuffer(512, 512);
    this.current_dry_fbo = 0;

    for (let i = 0; i < arrSize * arrSize; i++) {
      const x = i % arrSize | 0;
      const y = i / arrSize | 0;

      this.fbo_decals[i] = {
        fbo: new FrameBuffer(512, 512),
        x: x * this.size_quad | 0,
        y: y * this.size_quad | 0,
        time: 0
      };
    }

      this.shader_blit = new Shader(this.vert, this.fragBlit, [
      'mat_pos', 'tex'
    ]);
      this.shader_lerp = new Shader(this.vert, this.fragLerp, [
      'mat_pos', 'tex', 'tex_dest', 'color'
    ]);
      this.shader_add = new Shader(this.vert, this.fragAdd, [
      'mat_pos', 'tex', 'tex_dest', 'color'
    ]);
      this.shader_drying = new Shader(this.vert, this.fragDrying, [
      'mat_pos', 'tex'
    ]);
  }

   renderBase = (fbo, shader, texId, unit, secondId, color) => {
     fbo.bind();
     shader.use();
     shader.texture(shader.tex, texId, 0);

     if (shader.tex_dest && shader.color) {
       shader.texture(shader.tex_dest, secondId, 1);
       shader.vector(shader.color, color);
     }

     const mat = mat4.create();
     mat4Scal(mat, [1 / this.size_quad, 1 / this.size_quad]);

     const mb = mat4.create();
     mat4Trans(mb, [2 * (unit.pos.x - this.size_quad * 0.5), 2 * (-unit.pos.y + this.size_quad * 0.5)]);

     const mr = mat4.create();
     mat4Rotate(mr, unit.angle);
     mat4.mul(mb, mb, mr);
     mat4.mul(mat, mat, mb);

     mat4Scal(mat, unit.size.toVec());
     shader.matrix(shader.mat_pos, mat);
     game.gl.drawArrays(game.gl.TRIANGLE_STRIP, 0, 4);
     fbo.unbind();
   };

  renderAbsolute = (fbo, unit, tex, color, shAdd) => {
    const pos = unit.pos.clone().sub2(fbo.x, fbo.y);
    const rad = unit.size.length() * 0.5;
    if (pos.x + rad < 0) return;
    if (pos.y + rad < 0) return;
    if (pos.x - rad > this.size_quad) return;
    if (pos.y - rad > this.size_quad) return;
    if (!tex.getId()) return;

    const blend = game.gl.isEnabled(game.gl.BLEND);

    if (blend) {
      game.gl.disable(game.gl.BLEND);
    }

    const newUnit = {
      pos,
      size: unit.size,
      angle: unit.angle
    };

    this.renderBase(this.fbo_decal, this.shader_blit, fbo.fbo.getTexture(), newUnit);
    this.renderBase(fbo.fbo, shAdd ? this.shader_add : this.shader_lerp, tex.getId(), newUnit, this.fbo_decal.getTexture(), color);

    if (blend) {
      game.gl.enable(game.gl.BLEND);
    }
  };

  drying = (fbo) => {
    if (fbo.time < Date.now()) {
        fbo.time = Date.now() + 10;
      const dest = this.fbo_dry;
      dest.bind();
      this.shader_drying.use();
      this.shader_drying.matrix(this.shader_drying.mat_pos, mat4.create());
      this.shader_drying.texture(this.shader_drying.tex, fbo.fbo.getTexture(), 0);
      game.gl.drawArrays(game.gl.TRIANGLE_STRIP, 0, 4);
      dest.unbind();
      this.fbo_dry = fbo.fbo;
      fbo.fbo = dest;
      return true;
    }
    return false;
  };

  renderDecal = (unit, tex, color, shAdd) => this.fbo_decals.forEach((fbo) => this.renderAbsolute(fbo, unit, tex, color, shAdd));

  render = () => {
    this.fbo_decal.bind();
    this.fbo_decals.forEach((fbo) => {
        const pos = new Vector2D(fbo.x + (this.size_quad * 0.5), fbo.y + (this.size_quad * 0.5));
      const size = new Vector2D(this.size_quad, this.size_quad);

      renderUnit(fbo.fbo.getTexture(), levelDraw.shaderSimple, pos, size, 0, {not_use_visible: true});
    });
    this.fbo_decal.unbind();

    if (this.drying(this.fbo_decals[this.current_dry_fbo])) {
      this.current_dry_fbo++;

        if (this.current_dry_fbo > this.fbo_decals.length - 1) {
        this.current_dry_fbo = 0;
      }
    }
  };

  getTexture = () => this.fbo_decal.getTexture();
}

export const renderDecal = (unit, tex, color, sh_add = false) => levelDraw.getDecal().renderDecal(unit, tex, color, sh_add);
