import LevelGeneration from './generation';
import game from './../game';
import AI from './../ai/ai';
import Vector2D from './../helpers/vector';

export default class Level {
  size = 0;
  board = 5;
  seed = 2;
  level = null;

  constructor(size = 256, board = 1, seed = 42) {
    this.size = size;
    this.seed = seed;
    this.board = board;
    this.level = new LevelGeneration(this.size, board, seed);
    this.ai = new AI(this);
  }

  getAI = () => this.ai;

  getRandomPos = () => {
    while (true) {
      const x = this.board + (Math.random() * (this.level.getSize() - this.board - 1));
      const y = this.board + (Math.random() * (this.level.getSize() - this.board - 1));
      const pos = new Vector2D(x, y);
      if (!this.collideMap(pos, 50)) {
        return pos;
      }
    }
  };

  getSafetyDir = (pos) => {
      const ground = this.getCollide(pos, false);

    if (ground > 30) {
      const norm = new Vector2D(0, 0);
      this.getNorm(norm, pos, false);
      return norm.normalize();
    }

      const lava = this.getCollide(pos, true);

    if (lava > 30) {
      // bridges
      const collideBridge = this.collideBridges(pos);

      if (collideBridge) {
        const bridge = collideBridge.bridge;
        const bridgePos = collideBridge.pos;
        const norm = new Vector2D(0, 0);

        if (bridgePos.x > bridge.size.x * 0.5 - 0.3) norm.add2(-1, 0);
        if (bridgePos.x < -bridge.size.x * 0.5 + 0.3) norm.add2(1, 0);
        if (bridgePos.y > bridge.size.y * 0.5 - 0.3) norm.add2(0, -1);
        if (bridgePos.y < -bridge.size.y * 0.5 + 0.3) norm.add2(0, 1);

        const len = norm.dot(norm);

        norm.normalize().rotate(bridge.angle);
        return len < 0.5 ? null : norm;
      }

      const norm = new Vector2D(0, 0);
      this.getNorm(norm, pos, true);
      return norm.normalize();
    }

    return null;
  };

  frac = (x = 0) => x - (x | 0);

  lerp = (a, b, t) => (a * (1 - t)) + (b * t);

  getNorm = (dest, pos, river = false) => {
    const t00 = this.getCollide(pos, river);
    const t10 = this.getCollide(new Vector2D(pos.x + 0.25, pos.y), river);
    const t01 = this.getCollide(new Vector2D(pos.x, pos.y + 0.25), river);

    dest.set(t10 - t00, t01 - t00);

    return t00;
  };

  collideMap = (pos, factor = 80, river) => {
    const dir = new Vector2D(0, 0);
    const tile = this.getNorm(dir, pos, river);

    return tile > factor ? dir.mul(Math.abs(tile - factor)) : null;
  };

  collideRiver = (pos, factor = 160) => this.getCollide(pos, true) > factor;

  collideBridges = (pos) => {
    const bridges = game.level.level.getBridges().bridges;

    for (let i = 0; i < bridges.length; i++) {
      const bridge = bridges[i];
      const dist = pos.clone().sub(bridge.pos);

      const cosA = Math.cos(bridge.angle);
      const sinA = Math.sin(bridge.angle);

      const x = (dist.x * cosA) - (dist.y * sinA);
      const y = (dist.x * sinA) + (dist.y * cosA);

      if (Math.abs(x) < ((bridge.size.x * 0.5) + 0.3) && Math.abs(y) < ((bridge.size.y * 0.5) + 0.3)) {
        return {
          bridge,
          pos: new Vector2D(x, y)
        };
      }
    }

    return null;
  };

    collideUpdate = (tank) => {
    const norm = this.collideMap(tank.pos.clone().sub(tank.vel));

    if (norm) {
      const dot = norm.normalize().dot(tank.vel);

      if (dot > 0) {
          tank.pos.sub(norm.mul(dot));
      }
    }
  };

  getCollide = (pos, river) => {
    const buffer = river ? game.level.level.getRiverMap().getBuffer() : game.level.level.getGroundMap().getBuffer();
    const size = river ? game.level.level.getRiverMap().getSize() : game.level.level.getGroundMap().getSize();
    const getData = (x, y) => buffer[((y * size) + x) | 0];

    const x = pos.x - 0.25;
    const y = pos.y - 0.25;
    const cx = x | 0;
    const cy = y | 0;

    if (cx < 0) return 0;
    if (cy < 0) return 0;
    if (cx > size - 1) return 0;
    if (cy > size - 1) return 0;

    const t00 = getData(cx, cy);
    const t10 = getData(cx + 1, cy);
    const t01 = getData(cx, cy + 1);
    const t11 = getData(cx + 1, cy + 1);


    const dx = this.frac(x);
    const dy = this.frac(y);

    const xx1 = this.lerp(t00, t10, dx);
    const xx2 = this.lerp(t01, t11, dx);

    return (this.lerp(xx1, xx2, dy) * 255) | 0;
  };
}
