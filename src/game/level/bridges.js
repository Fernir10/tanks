import * as mat4 from 'gl-matrix/src/gl-matrix/mat4';
import {mat4Scal, mat4Trans} from './../../game/helpers/helpers';
import GameObject, {renderUnit} from '../../objects/gameobject';
import Shader from '../helpers/shader';
import Texture from '../helpers/texture';
import Vector2D from '../helpers/vector';
import game from './../game';
import levelDraw from './levelDraw';

const MIN_DIST = 32;

export class Bridge extends GameObject {
}

export default class Bridges {
    bridges = [];
    riverBuf = null;
    size = null;
    borderWidth = null;

    vert = `
    attribute vec4 position;
    uniform mat4 mat_pos;
    uniform mat4 mat_tex;
    varying vec4 texcoord;
    varying vec4 tc_visible;
    
    void main()
    {
        gl_Position = mat_pos * position;
        texcoord = mat_tex * position;
        texcoord.zw = position.xy * 0.5 + 0.5;
        tc_visible.xy = gl_Position.xy * 0.5 + 0.5;
    }`;

    frag = `
    #ifdef GL_ES
    precision highp float;
    #endif
    
    uniform sampler2D tex;
    uniform sampler2D texVisible;
    uniform sampler2D tex_mask;
    uniform sampler2D tex_decal;
    varying vec4 texcoord;
    varying vec4 tc_visible;
    
    void main()
    {
        vec4 col = texture2D(tex, texcoord.xy);
        vec4 visible = texture2D(texVisible, tc_visible.xy);
        vec4 decal = texture2D(tex_decal, tc_visible.xy);
        vec4 mask = texture2D(tex_mask, texcoord.zw);
        float shadow = clamp((1.0 - visible.g) * 6.0 - 3.0, 0.5, 1.0);
        
        col.rgb = mix(col.rgb, decal.rgb, decal.a);
        col.rgb *= (1.0 - visible.r) * shadow;
        gl_FragColor = col * mask.gggr;
    }`;

    fragShadow = `
    #ifdef GL_ES
    precision highp float;
    #endif
    
    uniform sampler2D tex;
    uniform sampler2D texVisible;
    uniform sampler2D tex_mask;
    uniform sampler2D tex_decal;
    varying vec4 texcoord;
    varying vec4 tc_visible;
    
    void main()
    {
        vec4 col = texture2D(tex, texcoord.xy);
        vec4 visible = texture2D(texVisible, tc_visible.xy);
        vec4 decal = texture2D(tex_decal, tc_visible.xy);
        vec4 mask = texture2D(tex_mask, texcoord.zw);
        float shadow = clamp((1.0 - visible.g) * 6.0 - 3.0, 0.5, 1.0);
        
        col.rgb = mix(col.rgb, decal.rgb, decal.a);
        col.rgb *= (1.0 - visible.r) * shadow;
        
        gl_FragColor = col * mask.gggr;
    }`;

    texture = new Texture('/img/metal.jpg');
    texMask = new Texture('/img/bridge_mask.png', {wrap: game.gl.CLAMP_TO_EDGE});

    shader = new Shader(this.vert, this.frag, [
      'mat_pos', 'mat_tex', 'tex', 'texVisible', 'tex_mask', 'tex_decal'
    ]);

    ready = () => this.texture.ready() && this.texMask.ready();

    render = () => {
      const texMaskID = this.texMask.getId();

      if (texMaskID === null) {
        return;
      }

      this.bridges.forEach((bridge) => {
        const matTex = mat4.create();

        mat4Trans(matTex, [0.5, 0.5]);
        mat4Scal(matTex, [(bridge.size.x / bridge.size.y) / (game.textureSize * 0.5), 1 / (game.textureSize * 0.5)]);
        bridge.render(this.texture, this.shader, {
          mat_tex: matTex,
          need_visible: true,
          textures: [
            {location: this.shader.tex_mask, id: texMaskID},
            {location: this.shader.tex_decal, id: levelDraw.getDecal().getTexture()}
          ]
        });
      });
    };

    getRiverWidth = (pos, dir) => {
      for (let step = 1; step < 8; step++) {
        const koef = this.riverBuf.getSize() / this.size;
        const p = pos.clone().add(dir.clone().mul(step)).mul(koef);
        const riverVal = this.riverBuf.getData(p.x | 0, p.y | 0);

        if (riverVal < 0.1) {
          return step;
        }
      }

      return 0;
    };

    createBridge = (prev, cur, next) => {
      let ret = null;
      const padding = this.borderWidth + 5;

      if (cur.x > padding && cur.x < this.size - padding &&
            cur.y > padding && cur.y < this.size - padding) {
        const a = prev.clone().sub(cur);
        const b = next.clone().sub(cur);

        let bissectrice = a.clone().add(b).mul(0.5);

        if (bissectrice.length() < 0.1) {
          bissectrice = a.clone().binormalize();
        }

        const normBiss = bissectrice.clone().normalize(bissectrice);
        const w1 = this.getRiverWidth(cur, normBiss);

        normBiss.mul(-1);

        const w2 = this.getRiverWidth(cur, normBiss);

        if (w1 > 0 && w2 > 0) {
          normBiss.mul(-1);

          const left = cur.clone().add(normBiss.clone().mul(w1));
          const half = normBiss.clone().mul(-(w1 + w2) * 0.5);
          const center = left.clone().add(half);

          ret = new Bridge(center.x, center.y, bissectrice.angle(), 0, new Vector2D(w1 + (w2 * 0.5), 3));
        }
      }

      return ret;
    };

    generateBridges = (river, length) => {
      let curLength = length;

      for (let i = 1; i < river.length - 1; i++) {
        curLength += river[i + 1].pos.clone().sub(river[i].pos).length();

        if (curLength > MIN_DIST) {
          const bridge = this.createBridge(river[i - 1].pos, river[i].pos, river[i + 1].pos);

          if (bridge) {
            curLength = 0;
            this.bridges.push(bridge);
          }
        }

        if (river[i].next) {
          this.generateBridges(river[i].next, curLength);
        }
      }
    };

    constructor(riverTree, riverBuffer, size, borderSize) {
      this.borderWidth = borderSize;
      this.size = size;
      this.riverBuf = riverBuffer;
      this.generateBridges(riverTree, 0);
    }
}
