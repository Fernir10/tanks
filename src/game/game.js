import ctx from './ctx';
import keyboard from './keyboard';
import levelDraw from './level/levelDraw';
import unitManager from '../objects/managers/unitmanager';
import Unit from '../objects/unit';
import Level from './level/level';
import Text from './helpers/text';
import {computeFPS} from './../game/helpers/helpers';
import {createSplash} from './helpers/particle';
import Vector2D from './helpers/vector';

class Game {
  gl = null;
  canvas = null;
  level = null;
  textureSize = 10;
  tankOffset = 0.25;
  camera = null;
  zPos = 0.5;
  text = null;
  dimension = 1;
    playerSpeed = 0.5;
    botSpeed = 0.4;

  // bots = () => unitManager.objects.filter((u) => u.isBot || u.isPlayer);
  // bots = () => unitManager.objects.filter((u) => u.isPlayer);
  bots = () => unitManager.objects.filter((u) => u.isBot);
  bullets = () => unitManager.objects.filter((u) => u.isBullet);

  gameHeight = () => window.innerHeight / this.dimension;
  gameWidth = () => window.innerWidth / this.dimension;

  animationLoop = () => {
    const player = unitManager.player;

    player.rotate = 0;
    player.direction.set(0, 0);
    player.shot = 0;
    player.isMoving = false;

    if (keyboard.keys && keyboard.keys[37]) {
      player.rotate = 5;
    }
    if (keyboard.keys && keyboard.keys[39]) {
      player.rotate = -5;
    }
    if (keyboard.keys && keyboard.keys[38]) {
      player.direction.add2(0, -player.speed);
      player.isMoving = true;
    }
    if (keyboard.keys && keyboard.keys[40]) {
      player.direction.add2(0, player.speed);
      player.isMoving = true;
    }
    if (keyboard.keys && keyboard.keys[32]) {
      player.shot = 1;
    }

    if (!unitManager.loaded()) {
      this.text.render([-0.9, 0], 1, '#rLoading', 2, {center: true});
    } else {
      unitManager.calculate();
      levelDraw.draw();
      unitManager.draw();
      levelDraw.renderMinimap();
      this.text.render([-0.9, 0.9], 2, `#rFPS: #w${computeFPS()}`, 2, {center: true});

      const bots = this.bots();

      bots
          .sort((a, b) => (b.killCount - b.deaths) - (a.killCount - a.deaths))
        .forEach((b, index) =>
            this.text.render([-0.98, 0.84 - index / 20], 1.2, `#r${b.nick}, #wkills: #g${b.killCount}#w, deaths: #g${b.deaths}, #wstate: #g${b.ai.state}`, 2)
        );
    }

    if (window.requestAnimationFrame) {
      window.requestAnimationFrame(this.animationLoop);
    }
  };

  init = () => {
    this.gl = ctx.init();
    this.canvas = ctx.canvas;
    this.text = new Text();
    this.text.render([0, 0], 1, '#rLoading', 2, {center: true});

      this.splashTextures = createSplash();

      this.level = new Level(64, 4, 0);
      keyboard.init();
      levelDraw.init();
      unitManager.init();

      this.camera = unitManager.player;

      for (let i = 0; i < this.level.level.getSize() / 8; i++) {
          const vec = this.level.getRandomPos();
          unitManager.add(new Unit(vec.x, vec.y, 0.1, Math.random() * 10, new Vector2D(1, 1), true));
      }

    this.animationLoop();
  };
}

const game = new Game();

export default game;
